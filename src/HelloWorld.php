<?php

/**
 * Class HelloWorld.
 *
 * @package    Laughing_man77
 * @subpackage Processor
 * @link       https://gitlab.com/laughing_man77
 */

namespace Laughing_man77;

use ApiOpenStudio\Core\ApiException;
use ApiOpenStudio\Core\DataContainer;
use ApiOpenStudio\Core\ProcessorEntity;

/**
 * Class HelloWorld
 *
 * Processor class to return Hello world! text.
 */
class HelloWorld extends ProcessorEntity
{
    /**
     * {@inheritDoc}
     *
     * @var array Details of the processor.
     */
    protected array $details = [
        'name' => 'Hello world!',
        'machineName' => '\\Laughing_man77\\HelloWorld',
        'description' => 'Returns "Hello world!"',
        'menu' => 'Primitive',
        'input' => [],
    ];

    /**
     * {@inheritDoc}
     *
     * @return DataContainer Result of the processor.
     *
     * @throws ApiException Exception if invalid result.
     */
    public function process(): DataContainer
    {
        parent::process();

        return new DataContainer('Hello world!', 'text');
    }
}
